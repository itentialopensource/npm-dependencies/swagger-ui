
## 3.22.3 [10-25-2019]

* Patch/security update

See merge request itentialopensource/npm-dependencies/swagger-ui!1

---
## 3.22.2 [04-09-2019]
* Bug fixes and performance improvements

See commit 3081d5fb

---

## 3.22.1 [04-09-2019]
* Bug fixes and performance improvements

See commit 94a9ba3b

---
